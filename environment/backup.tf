resource "kubernetes_cron_job_v1" "boursoloco_backup" {
  metadata {
    name      = "boursoloco-backup"
    namespace = module.namespace.namespace
  }

  spec {
    concurrency_policy            = "Replace"
    failed_jobs_history_limit     = 10
    schedule                      = "35 3 * * *"
    starting_deadline_seconds     = 10
    successful_jobs_history_limit = 5

    job_template {
      metadata {}

      spec {
        backoff_limit = 3

        template {
          metadata {}
          spec {
            container {
              name  = "mariadb-backup"
              image = "registry.gitlab.com/qonfucius/infrastructure/docker-images/mariadb-backup-alpine:latest"

              resources {
                limits = {
                  cpu    = "500m"
                  memory = "512Mi"
                }
              }

              env {
                name  = "NAME"
                value = "boursoloco"
              }

              env {
                name  = "S3_REGION"
                value = var.backup_bucket_region
              }

              env {
                name  = "S3_ENDPOINT_URL"
                value = "https://${var.backup_bucket_name}.s3.${var.backup_bucket_region}.scw.cloud"
              }

              env {
                name  = "S3_ACCESS_KEY"
                value = var.scaleway_access_key
              }

              env {
                name  = "S3_SECRET_KEY"
                value = var.scaleway_secret_key
              }

              env {
                name  = "MYSQL_HOST"
                value = "mariadb-mariadb.${module.namespace.namespace}"
              }

              env {
                name  = "MYSQL_USER"
                value = "root"
              }

              env {
                name  = "MYSQL_PASSWORD"
                value = module.mariadb.root_password
              }
            }
          }
        }
      }
    }
  }
}
