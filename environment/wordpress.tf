module "wordpress" {
  source                      = "gitlab.com/vigigloo/tools-k8s/wordpress"
  version                     = "1.0.0"
  chart_name                  = "wordpress"
  chart_version               = "16.1.6"
  image_tag                   = "6.2.2-debian-11-r82"
  namespace                   = module.namespace.namespace
  wordpress_persistence_size  = "10Gi"
  wordpress_database_username = local.mariadb_username
  wordpress_database_database = local.mariadb_database
  wordpress_database_host     = module.mariadb.host
  wordpress_database_password = resource.random_password.mariadb_wordpress_password.result
  wordpress_database_port     = "3306"
  values = [
    file("${path.module}/wordpress.yaml"),
    <<-EOT
    ingress:
      enabled: true
      annotations:
        acme.cert-manager.io/http01-edit-in-place: "true"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/ingress.class: haproxy
      hostname: ${var.wordpress_host}
      pathType: Prefix
      path: /
      extraTls:
        - hosts:
            - ${var.wordpress_host}
          secretName: wordpress-tls
    EOT
  ]
}
