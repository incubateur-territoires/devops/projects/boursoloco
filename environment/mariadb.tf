locals {
  mariadb_database = "wordpress"
  mariadb_username = "wordpress"
}
resource "random_password" "mariadb_wordpress_password" {
  length  = 32
  special = false
}
module "mariadb" {
  source                = "gitlab.com/vigigloo/tools-k8s/mariadb"
  version               = "0.1.1"
  chart_name            = "mariadb"
  namespace             = module.namespace.namespace
  values                = [file("${path.module}/mariadb.yaml")]
  mariadb_auth_username = local.mariadb_username
  mariadb_auth_password = resource.random_password.mariadb_wordpress_password.result
  mariadb_auth_database = local.mariadb_database
}
